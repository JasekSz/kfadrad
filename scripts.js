var horizontal = 1;
var vertical = 1;
var quantity = 10;
var coordinatesX = [];
var coordinatesY = [];
var score = 0;
var oldScore = 0;
var food = [];

score = quantity;
oldScore = score;
$('#score').html('<p>pozostało jeszcze: ' + score+ ' kropek</p>');
$(window).keypress(function(e) {
    if(oldScore == score-1){
        oldScore--;
        AnimateRotate(360);
    }
    if(horizontal>0 && vertical>0 && horizontal<300 && vertical<300) {
        // ruszanie w prawo, klawisz "d"

        if (e.which === 100) {
            horizontal = horizontal + 5;
            moveHorizontal(horizontal);
            $("img").css("transform", "scaleX(-1)");
        }
        // ruszanie w lewo, klawisz "a"
        if (e.which === 97) {
            horizontal = horizontal - 5;
            moveHorizontal(horizontal);
            $("img").css("transform", "scaleX(1)");
        }
        // ruszanie w dół klawisz "s"
        if (e.which === 115) {
            vertical = vertical + 5;
            moveVertical(vertical);
        }
        // ruszanie w górę klawisz "w"
        if (e.which === 119) {
            vertical = vertical - 5;
            moveVertical(vertical);
        }
    } else {
        if(horizontal<0) {
            horizontal = horizontal + 5;
        }
        if(horizontal>300) {
            horizontal = horizontal - 5;
        }
        if(vertical>300) {
            vertical = vertical - 5;
        }
        if(vertical<0) {
            vertical = vertical + 5;
        }
    }
    for(let i = 0; i <= quantity; i++){
        if (vertical < coordinatesY[i] && vertical > coordinatesY[i] - 50 && horizontal < coordinatesX[i] && horizontal > coordinatesX[i] - 50) {
            $('#c' + i).remove();
        }
    }
    score = $("#board").children().length - 1;
    $('#score').html('<p>pozostało jeszcze: ' + score+ ' kropek</p>');

    function AnimateRotate(angle) {
        // caching the object for performance reasons
        var $elem = $('#klocek');

        // we use a pseudo object for the animation
        // (starts from `0` to `angle`), you can name it as you want
        $({deg: 0}).animate({deg: angle}, {
            duration: 2000,
            step: function(now) {
                // in the step-callback (that is fired each step of the animation),
                // you can use the `now` paramter which contains the current
                // animation-position (`0` up to `angle`)
                $elem.css({
                    transform: 'rotate(' + now + 'deg)'
                });
            }
        });
    }
   // AnimateRotate(360);


    if(score==0){
        var audio = new Audio('sounds/malekoty.mp3');
        audio.play();
        function AnimateRotate(angle) {
            // caching the object for performance reasons
            var $elem = $('#klocek');

            // we use a pseudo object for the animation
            // (starts from `0` to `angle`), you can name it as you want
            $({deg: 0}).animate({deg: angle}, {
                duration: 2000,
                step: function(now) {
                    // in the step-callback (that is fired each step of the animation),
                    // you can use the `now` paramter which contains the current
                    // animation-position (`0` up to `angle`)
                    $elem.css({
                        transform: 'rotate(' + now + 'deg)'
                    });
                }
            });
        }

    }
});

    var audio = new Audio('sounds/chrupanie.wav');
    audio.play();

function getRandom() {
   return Math.floor(Math.random()*300);
}

function moveHorizontal(i){
    $('#klocek').css("left", i+"px");
}
function moveVertical(i){
    $('#klocek').css("top", i+"px");
}

function createFood(quantity){
    for(let i = 0; i <= quantity; i++){
        var elementName = 'c' + i;
        $('#board').append('<div class="food" id="'+ elementName + '"</div>');
        food[i]=i;
    }
    for(let i = 0; i <= quantity; i++){
        var elementName = '#c' + i;
        var x = getRandom();
        var y = getRandom();
        if(x<50&&y<50){
            x=x+50;
            y=y+50;
        }
        coordinatesX[i]=x;
        coordinatesY[i]=y;
        $(elementName).css("left", x + "px");
        $(elementName).css("top", y + "px");
    }

}
createFood(quantity);






